class Address < ActiveRecord::Base
  has_many :owners

  def full_address
    "#{street}, #{city}, #{state}"
  end
end
