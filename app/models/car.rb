class Car < ActiveRecord::Base
  belongs_to :owner

  def details
    "#{make} #{model}"
  end
end
